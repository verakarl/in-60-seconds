---?image=assets/img/logo.png&position=left&size=55% 100%

@title[Intro]

## @color[#ffffff](HAXE)
<br>
#### cross-platform tool targeting all the mainstream platforms @color[#e49436](natively)

---

Basic Types: Int float Bool

Interface = structural info
jedes field braucht type und function dont have an expression, beschreiben statische beziehung zw. Klassen
kann mehrere interfaces implementieren

+++?image=template/img/bg/orange.jpg&position=right&size=50% 100% @title[Heading + List Body]

@snap[west text-16 text-bold text-italic text-orange span-50] Topics to be covered today @snapend

@snap[east text-white span-45] @olsplit-screen-list text-08

Lorem ipsum dolor sit amet, consectetur elit
Ut enim ad minim veniam, quis exercitation
Duis aute irure dolor in reprehenderit in voluptate @olend @snapend
@snap[south-west template-note text-gray] Split-screen heading and list body template. @snapend



